#include <Arduino.h>


//#include <Ticker.h>  //Ticker Library
//
// Voltimetro_dc.ino
// hispavila.com
// Construcción de un voltímetro DC con Arduino
// 20.09.2015
// Utiliza el monitor Serial para mostrar los valores.
// VCC-----|R1|---(A0)---|R2|------GND
//


class ControlValvula{ 
    private:
        unsigned long _time =0;
        unsigned long _timeOn = 0 ;
        unsigned long _timeOff = 0 ;
        bool _stateDoor = false;
        bool _available = false;
        byte _door = 0;
        unsigned long _dateRefresh = 0;
        byte _delayVal = 15;
        uint_8_t _puenteA = 0;
        uint_8_t _puenteb = 0;
        int _releOn = 1;
        int _releOff = 0 ;
        
    public:
        ///TODO POR HACER
        ControlValvula()
        {
            _available = false;
            _time = millis();
        }
        ControlValvula( uint8_t puenteA ,uint8_t puenteB ,byte delayValvula,bool available, int minutesOn,int secondOn, int minutesOff,int secondOff)
        {
            _door = door;
            _available = available;
            _timeOn =  minutesOn*(uint32)60e3+secondOn*(uint32)1e3;
            _timeOff = minutesOff*(uint32)60e3+secondOff*(uint32)1e3;
            _delayVal = delayValvula;
            _puenteA = puenteA;
            _puenteB = puenteB;
            _time = millis();

        }
       /* ControlValvula( byte door ,byte delayValvula,bool available, int minutesOn,int secondOn, int minutesOff,int secondOff)
        {
            _door = door;
            _available = available;
            _timeOn =  minutesOn*(uint32)60e3+secondOn*(uint32)1e3;
            _timeOff = minutesOff*(uint32)60e3+secondOff*(uint32)1e3;
            _delayVal = delayValvula;
            _time = millis();

        }*/
        void loop()
        {
            if(_available){
                if(_stateDoor){
                    if(millis()>_timeOn+_time){
                        //Tiempo de riego transcurrido
                        ChageStateValveAndReset();                       
                    }
                }else{
                    if(millis()>_timeOff+_time){
                        //Tiempo de sequía transcurrido
                        ChageStateValveAndReset();                
                    }
                }
            }

        }
        void SetAvailable(bool available){_available = available;}
        void CloseValve()
        {
            if(_stateDoor == true){CloseDoor((int)_door,(int)_delayVal);}
        }
        void OpenValve()
        {
            if(_stateDoor == false){OpenDoor((int)_door,(int)_delayVal);}
        }
        void ChageStateValve()
        {
             if(_stateDoor == false){OpenDoor((int)_door,(int)_delayVal);}else{CloseDoor((int)_door,(int)_delayVal);}
             _stateDoor = !_stateDoor;
        }
        void ChageStateValveAndReset()
        {
             if(_stateDoor == false){OpenDoor((int)_door,(int)_delayVal);}else{CloseDoor((int)_door,(int)_delayVal);}
             _stateDoor = !_stateDoor;
             _time = millis();
        }
        
    protected:
        void OpenDoor(int door,int retraso)
        {
            pinMode(_puenteA, OUTPUT);
            pinMode(_puenteB, OUTPUT);
            digitalWrite(_puenteA, _releOn);
            digitalWrite(_puenteB, _releOff);
            delay(retraso);
            digitalWrite(_puenteA, _releOff);
            digitalWrite(_puenteB, _releOff);
        /*
        digitalWrite(LED_BUILTIN, LOW);
        Serial.print("OPEN door-->"); Serial.println(door);
        Serial.print("delay--> "); Serial.println(retraso);
            
        
            if(door==0){
                pinMode(puente01A1, OUTPUT);
                pinMode(puente01A2, OUTPUT);
                digitalWrite(puente01A1, rele_on);
                digitalWrite(puente01A2, rele_off);
                delay(retraso);
                digitalWrite(puente01A1, rele_off);
                digitalWrite(puente01A2, rele_off);
            }
            if(door==1){
                pinMode(puente01B1, OUTPUT);
                pinMode(puente01B2, OUTPUT);
                digitalWrite(puente01B1, rele_on);
                digitalWrite(puente01B2, rele_off);
                delay(retraso);
                digitalWrite(puente01B1, rele_off);
                digitalWrite(puente01B2, rele_off);
            }
                if(door==2){
                pinMode(puente02A1, OUTPUT);
                pinMode(puente02A2, OUTPUT);
                digitalWrite(puente02A1, rele_on);
                digitalWrite(puente02A2, rele_off);
                delay(retraso);
                digitalWrite(puente02A1, rele_off);
                digitalWrite(puente02A2, rele_off);
            }
                if(door==3){
                pinMode(puente02B1, OUTPUT);
                pinMode(puente02B1, OUTPUT);
                digitalWrite(puente02B1, rele_on);
                digitalWrite(puente02B2, rele_off);
                delay(retraso);
                digitalWrite(puente02B1, rele_off);
                digitalWrite(puente02B2, rele_off);
            }
            */
        }
        void CloseDoor(int door,int retraso)
        {
            pinMode(_puenteA, OUTPUT);
            pinMode(_puenteB, OUTPUT);
            digitalWrite(_puenteA, _releOff);
            digitalWrite(_puenteB, _releOn);
            
            delay(retraso);
            digitalWrite(_puenteA, _releOff);
            digitalWrite(_puenteB, _releOff);
            /*
            digitalWrite(LED_BUILTIN, HIGH);
            Serial.print("close door-->"); Serial.println(door);
            Serial.print("delay--> "); Serial.println(retraso);
            
        if(door==0){
                pinMode(puente01A1, OUTPUT);
                pinMode(puente01A2, OUTPUT);
                digitalWrite(puente01A1, rele_off);
                digitalWrite(puente01A2, rele_on);
                
                delay(retraso);
                digitalWrite(puente01A1, rele_off);
                digitalWrite(puente01A2, rele_off);
        }if(door==1){
                pinMode(puente01B1, OUTPUT);
                pinMode(puente01B2, OUTPUT);
                digitalWrite(puente01B1, rele_off );
                digitalWrite(puente01B2, rele_on);
                delay(retraso);
                digitalWrite(puente01B1, rele_off);
                digitalWrite(puente01B2, rele_off);
            }
                if(door==2){
                pinMode(puente02A1, OUTPUT);
                pinMode(puente02A2, OUTPUT);
                digitalWrite(puente02A1, rele_off );
                digitalWrite(puente02A2,rele_on );
                delay(retraso);
                digitalWrite(puente02A1, rele_off);
                digitalWrite(puente02A2, rele_off);
            }
                if(door==3){
                pinMode(puente02B1, OUTPUT);
                pinMode(puente02B2, OUTPUT);
                digitalWrite(puente02B1,rele_off );
                digitalWrite(puente02B2, rele_on);
                delay(retraso);
                digitalWrite(puente02B1, rele_off);
                digitalWrite(puente02B2, rele_off);
            }
        }
        */

  } ;




