#include "config.hpp"
#include <ESP8266WiFi.h>
//#include "DHT.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>//V6.03
#include "utils.hpp"
#include "voltimetro.hpp"
#include "accessPoint.hpp"
#include "controlValve.hpp"

#if DHT_MODE
  #include "DHT.h"
  DHT dht(DHTPin, DHTTYPE);
#endif

#define DEBUG 1


WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
bool st = true;
unsigned long tiempo,tiempo2,tiempo3;
bool _modeAccesPoint = false;
#if VOLTIMETRO_MODE
	Voltimetro voltimetro(1000000,_tiempo_Refresh_ADC);//voltage r1,time refres
#endif
#if SENSOR_SUELO_MODE
    int contADC = 0;
    float ADCMedia = 0;
#endif

bool _continue = true;
float volOld = 0;
ControlValvula _valve0,_valve1,_valve2,_valve3;
void SendData()
{
	#if VOLTIMETRO_MODE 
		float vol = voltimetro.getMedia();
	#endif
	#if SENSOR_SUELO_MODE
		float humSuelo = ADCMedia/(float)contADC;
		float vol = map(humSuelo,0,1023,0,100);
		ADCMedia =0;
		contADC =0;
	#endif
	
	#if DHT_MODE
	delay(dht.getMinimumSamplingPeriod());

	float h = dht.getHumidity();
	float t = dht.getTemperature();
	Serial.println("Collecting temperature & voltaje data.");


	// Check if any reads failed and exit early (to try again).
	if (isnan(h) || isnan(t)) {
		Serial.println("Failed to read from DHT sensor!");
		h = 0.0;
		t = 0.0;
		//return;
	}
	String temperature = String(t);
	String humidity = String(h);

	#endif
	
	if(vol != volOld){
		volOld = vol;
		String payload ;
		payload ="{";
		#if VOLTIMETRO_MODE 
			payload += "\"voltajeBatt\":"; payload += volOld;
		#endif
		#if SENSOR_SUELO_MODE
			payload += "\"humedadSuelo\":"; payload += volOld;
		#endif

		#if DHT_MODE
			payload += ",";
			payload += "\"temperature\":"; payload += temperature; payload += ",";
			payload += "\"humidity\":"; payload += humidity; 
		#endif
		//payload += "\"retraso\":"; payload += delayRele;payload += ",";
		
		payload += "}";
		#if DEBUG
			Serial.println(payload);
		#endif
		char attributes[200];
		payload.toCharArray( attributes, 200 );
		
		if(!client.publish(controlRiego, attributes))
		{Serial.println("SendMSG false");}
		}
  }

uint32_t GetMicrosecond(int minutos)
{
	uint32_t res = minutos *(uint32)(60e6);
		return res;
}
void SleepEsp(int minutos)
{
	uint32_t timeSleep =GetMicrosecond(minutos);
	#if DEBUG
				Serial.print("timeSleep: ");Serial.println(timeSleep);
			#endif
	//ESP.deepSleep(timeSleep,WAKE_RF_DEFAULT);
	ESP.deepSleep(timeSleep);
}
void Refresh_Time()
	{
		
		if(config.decremento_minutos==0){config.decremento_minutos =1;}
		#if DEBUG
			Serial.print("config.counterOn_0");
			Serial.println(config.counterOn_0);
			Serial.print("config.counterOff_0");
			Serial.println(config.counterOff_0);
			Serial.print("config.decremento_minutos");
			Serial.println(config.decremento_minutos);
			Serial.print("config.decremento_minutos");
			Serial.println(config.decremento_minutos);
		#endif
		//ciclo riego 0	
		if(config.state_riego_0){
			if(config.power_0==true)
			{
				Serial.println("riego");
				if(config.counterOn_0>=config.decremento_minutos)
				{
					Serial.println("riego si entro");
					config.counterOn_0 = config.counterOn_0-config.decremento_minutos;
				}
				else
				{
					Serial.println("aqui riego no entro");
					config.power_0 = !config.power_0;
					config.counterOn_0 = config.timeOn_0;
				}
			}
			//ciclo seco 0
			else{
				Serial.println("No riego");
				if(config.counterOff_0>config.decremento_minutos)
				{
					Serial.println("aqui si entro");
					config.counterOff_0 = config.counterOff_0-config.decremento_minutos;
				}
				else
				{
					Serial.println("aqui no entro");
					config.power_0 = !config.power_0;
					config.counterOff_0 =config.timeOff_0;
				}
			}
		}
		//ciclo riego 1	
		if(config.state_riego_1){
			if(config.power_1==true)
			{
				if(config.counterOn_1>=config.decremento_minutos)
				{
					config.counterOn_1 = config.counterOn_1-config.decremento_minutos;
				}
				else
				{
					config.power_1 = !config.power_1;
					config.counterOn_1 = config.timeOn_1;
				}
			}
			//ciclo seco 1
			else{
				if(config.counterOff_1>=config.decremento_minutos)
				{
					config.counterOff_1 = config.counterOff_1-config.decremento_minutos;
				}
				else
				{
					config.power_1 = !config.power_1;
					config.counterOff_1 =config.timeOff_1;
				}
			}
		}
		//ciclo riego 2
		if(config.state_riego_2){
			if(config.power_2==true)
			{
				if(config.counterOn_2>=config.decremento_minutos)
				{
					config.counterOn_2 = config.counterOn_2-config.decremento_minutos;
				}
				else
				{
					config.power_2 = !config.power_2;			
					config.counterOn_2 = config.timeOn_2;
					
				}
			}
				//ciclo seco 2
			else{
				if(config.counterOff_2>=config.decremento_minutos)
				{
					config.counterOff_2 = config.counterOff_2-config.decremento_minutos;
				}
				else
				{
					config.power_2 = !config.power_2;
					config.counterOff_2 =config.timeOff_2;
				
				}
			}
		}
		//ciclo riego 3
		if(config.state_riego_3){	
			if(config.power_3==true)
			{
				if(config.counterOn_3>=config.decremento_minutos)
				{
					config.counterOn_3 = config.counterOn_3-config.decremento_minutos;
				}
				else
				{
					config.power_3 = !config.power_3;
					config.counterOn_3 = config.timeOn_3;
				}
		}
			//ciclo seco 3
			else{
			if(config.counterOff_3>=config.decremento_minutos)
			{
				config.counterOff_3 = config.counterOff_3-config.decremento_minutos;
			}
			else
			{
				config.power_3 = !config.power_3;
				config.counterOff_3 =config.timeOff_3;
			}
		}
		}
		
		//Get min time for sleep
		#if DEBUG
		Serial.println("---------------------");
		Serial.print("Decremento restante: ");Serial.println(config.decremento_minutos);
		#endif
		int timeSleep = 70;//time maximo for sleep= 71+-= 70 + millis()
		if(config.state_riego_0 == true){
			if(timeSleep>config.counterOff_0){timeSleep=config.counterOff_0;}
			if(timeSleep>config.counterOn_0){timeSleep=config.counterOn_0;}
		}
		if(config.state_riego_1 == true){
			if(timeSleep>config.counterOff_1){timeSleep=config.counterOff_1;}
			if(timeSleep>config.counterOn_1){timeSleep=config.counterOn_1;}
		}
		if(config.state_riego_2 == true){
			if(timeSleep>config.counterOff_2){timeSleep=config.counterOff_2;}
			if(timeSleep>config.counterOn_2){timeSleep=config.counterOn_2;}
		}
		if(config.state_riego_3 == true){
			if(timeSleep>config.counterOff_3){timeSleep=config.counterOff_3;}
			if(timeSleep>config.counterOn_3){timeSleep=config.counterOn_3;}
		}
		config.decremento_minutos = timeSleep;	
		#if DEBUG
		Serial.print("Calculado decremeto : ");Serial.println(timeSleep);
		Serial.println("---------------------");
		#endif

		
		#if DEBUG
		Serial.print("config.counterOn_0");
		Serial.println(config.counterOn_0);
		Serial.print("config.counterOff_0");
		Serial.println(config.counterOff_0);
		Serial.print("config.decremento_minutos");
		Serial.println(config.decremento_minutos);
		Serial.print("config.decremento_minutos");
		Serial.println(config.decremento_minutos);
		#endif
		
		uint32_t timeDeepSleep = GetMicrosecond(timeSleep)+(uint32)(millis()*1e3);//minutos por microsegundos de un minuto mas el tiempo de ejecucción
		eepromsave();
		#if DEBUG
			Serial.print("Save minute time sleep: ");Serial.println(config.decremento_minutos);
			Serial.println("--------------");
			Serial.print("Calculate Deepsleep (microsecond) : ");Serial.println(timeDeepSleep);
			uint32_t calS =timeDeepSleep/(uint32)(1e6);
			Serial.print("Seconds sleep : ");Serial.println(calS);
			uint32_t calM =calS/60;
			Serial.print("Minte sleep : ");Serial.println(calM);
			Serial.println("--------------");
			delay(1000);
		#endif
		ESP.deepSleep(timeSleep,WAKE_RF_DISABLED);
			

		}
void Setup_wifi() {

	//delay(10);
	// We start by connecting to a WiFi network
	int tope = 0;
	WiFi.begin(config.wifi, config.pass);
	do{
		
		delay(250);
		Serial.print(".");
		tope++;
		yield();
	
	}while (WiFi.status() != WL_CONNECTED && tope < 10) ;
	#if DEBUG
	Serial.println("");
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());
	#endif
}
void callback(char* topic, byte* payload, unsigned int length)
{
	Serial.print("Message recibido [");
	Serial.print(topic);
	Serial.print("] ");
	for (int i = 0; i < length; i++) {
		Serial.print((char)payload[i]);
	}
	Serial.println();
	//JSON
	StaticJsonDocument<200> doc;
	DeserializationError error = deserializeJson(doc, payload);

	// Test if parsing succeeds.
	if (error) {
		Serial.print(F("deserializeJson() failed: "));
		Serial.println(error.c_str());
		return;
	}
	
	//{"mode":1,"door0": "on","door2":"off","retraso":15,"sleep":10,"door0On":1,"door0Off":71,"changeDoor0"=true}
	char modeAu = doc["mode"];
	if (modeAu != 0) {
		config.mode = modeAu;
		Serial.print("mode recived: "); Serial.println(config.mode);
	}
	int retrasoAu = doc["retraso"];
	if (retrasoAu != 0) {
		delayRele = retrasoAu;
		Serial.print("retraso recived: "); Serial.println(delayRele);
	}
	//////////////////////////
	String door0 = doc["door0"];
	if (door0 =="on") {
		//OPENDoor(0,delayRele);
		_valve0.OpenValve();
		Serial.print("OPEN door0 recived: "); Serial.println(door0);
	}
	if (door0 =="off") {	
		//CLOSEDoor(0,delayRele);
		_valve0.CloseValve();
		Serial.print("CLOSE door0  recived: "); Serial.println(door0);
	}
	int door0On = doc["door0On"];
	if(door0On>0)
	{
		config.timeOn_0 = door0On;
		Serial.print("config.timeOn_0 configured: "); Serial.println(config.timeOn_0);
		eepromsave();
		
	}
	int door0Off = doc["door0Off"];
	if(door0Off>0)
	{
		config.timeOff_0 = door0Off;
		Serial.print("config.timeOff_0 configured: "); Serial.println(config.timeOff_0);
		eepromsave();
		
	}
	bool changeDoor0 = doc["changeDoor0"];
	if(changeDoor0)
	{
		_valve0.ChageStateValveAndReset();
	}
	///////////////////
	String door1 = doc["door1"];
	if (door1 =="on") {
		//OPENDoor(1,delayRele);
		_valve1.OpenValve();
		Serial.print("OPEN door1 recived: "); Serial.println(door1);
	}
	if (door1 =="off") {
		//CLOSEDoor(1,delayRele);
		_valve1.CloseValve();
		Serial.print("CLOSE door1 recived: "); Serial.println(door1);	
	}
	int door1On = doc["door1On"];
	if(door1On>0)
	{
		config.timeOn_1 = door1On;
		Serial.print("config.timeOn_1 configured: "); Serial.println(config.timeOn_1);
		eepromsave();
	}
	int door1Off = doc["door1Off"];
	if(door1Off>0)
	{
		config.timeOff_1 = door1Off;
		Serial.print("config.timeOff_1 configured: "); Serial.println(config.timeOff_1);
		eepromsave();
		
	}
	bool changeDoor1 = doc["changeDoor1"];
	if(changeDoor1)
	{
		_valve1.ChageStateValveAndReset();
	}
	//////////////DOOR"
	String door2 = doc["door2"];
	if (door2 =="on") {
		//OPENDoor(2,delayRele);
		_valve2.OpenValve();
		Serial.print("OPEN door2 recived: "); Serial.println(door2);
	}
	if (door2 =="off") {
		//CLOSEDoor(2,delayRele);
		_valve2.CloseValve();
		Serial.print("CLOSE door2 recived: "); Serial.println(door2);
	}
	int door2On = doc["door2On"];
	if(door2On>0)
	{
		config.timeOn_2 = door2On;
		Serial.print("config.timeOn_2 configured: "); Serial.println(config.timeOn_2);
		eepromsave();
	}
	int door2Off = doc["door2Off"];
	if(door2Off>0)
	{
		config.timeOff_2 = door2Off;
		Serial.print("config.timeOff_2 configured: "); Serial.println(config.timeOff_2);
		eepromsave();
	}
	bool changeDoor2 = doc["changeDoor2"];
	if(changeDoor2)
	{
		_valve2.ChageStateValveAndReset();
	}
	/////////////////////////////////
	String door3 = doc["door3"];
	if (door3 =="on") {
		//OPENDoor(3,delayRele);
		_valve3.OpenValve();
		Serial.print("OPEN door3 recived: "); Serial.println(door3);	
	}
	if (door3 =="off") {
		//CLOSEDoor(3,delayRele);
		_valve3.CloseValve();
		Serial.print("CLOSE door3 recived: "); Serial.println(door3);
		
	}
	int door3On = doc["door3On"];
	if(door3On>0)
	{
		config.timeOn_3 = door3On;
		Serial.print("config.timeOn_3 configured: "); Serial.println(config.timeOn_3);
		eepromsave();
	}
	int door3Off = doc["door3Off"];
	if(door3Off>0)
	{
		config.timeOff_3 = door3Off;
		Serial.print("config.timeOff_3 configured: "); Serial.println(config.timeOff_3);
		eepromsave();
	}
	bool changeDoor3 = doc["changeDoor3"];
	if(changeDoor3)
	{
		_valve3.ChageStateValveAndReset();
	}
	/////////////////////////////////
	int sleep = doc["sleep"];
	if(sleep>0)
	{
		Serial.print("minutos sleep recived: "); Serial.println(sleep);
		SleepEsp(sleep);
	}
}

void reconnect() {
	// Loop until we're reconnected
	if (!client.connected() ) {
		Serial.print("Attempting MQTT connection...");
		// Attempt to connect
		if (client.connect(nameNode)) {
			Serial.println("connected");
			// Enviamos los datos obtenidos
			
			if(!client.publish(controlRiego, nameNode))
			{
				Serial.println("Msg error send");
			}
			// ... and resubscribe
			client.subscribe(actionRiego);
		}
		else {
			
			//delay(50);
			#if DEBUG
				Serial.print("failed, rc=");
				Serial.print(client.state());
				Serial.println(" try again in 5 seconds");
			// Wait 5 seconds before retrying
			#endif // DEBUG
			
		}	
		
	}
}


void setup() {
	//pinMode(LED_BUILTIN, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
	//digitalWrite(LED_BUILTIN, HIGH);

	//pinMode(pinButton,OUTPUT);
	//digitalWrite(pinButton,LOW);
	
	pinMode(pinButton,INPUT);
	Serial.begin(115200);
	eeprominit();
	delay(200);
	
	eepromload();
	_valve0 = ControlValvula(D1,D2,delayRele,config.state_riego_0,config.counterOn_0,0,config.counterOff_0,0);
	_valve1 = ControlValvula(D3,D4,delayRele,config.state_riego_1,config.counterOn_1,0,config.counterOff_1,0);
	_valve2 = ControlValvula(D5,D6,delayRele,config.state_riego_2,config.counterOn_2,0,config.counterOff_2,0);
	_valve3 = ControlValvula(D7,D8,delayRele,config.state_riego_3,config.counterOn_3,0,config.counterOff_3,0);
	if(digitalRead(pinButton)==HIGH){
    	_modeAccesPoint = true;
 	 }
	  tiempo3 = millis();
	  tiempo2 = millis();
	  tiempo = millis();
	if(_modeAccesPoint == true){
		
		
		while(millis()<=((unsigned long)60e3*5)+tiempo)//60 segundos por 5 = 5 minutos
		{	
			if(_continue)
			{	
				_continue = false;
				Serial.print("REFRESH modo AccesPoint + STA");
				obtenerWifis();
				accecssPoint();
				//WiFi.mode(WIFI_STA);
				//WiFi.mode(WIFI_AP_STA);
			}
			server.handleClient();
			
		}
	}
	Serial.print("config mode ->");Serial.println(config.mode);
	if(config.mode==1 || config.mode==2){
		#if DHT_MODE
			dht.begin();
		#endif
		
		Setup_wifi();
		#if DEBUG
			Serial.print("SetServer mqtt-->");
			Serial.print(mqtt_server);Serial.print(":");Serial.println(mqtt_port);
		#endif
		client.setServer(mqtt_server, mqtt_port);
		client.setCallback(callback);
		}
	
		
		#if VOLTIMETRO_MODE
			voltimetro.VoltimetroInfo();
		#endif
		#if SENSOR_SUELO_MODE
			
		#endif
		tiempo = millis();
		tiempo2 = millis();
		tiempo3 = millis();
		
		
}

void loop() {
	switch (config.mode)
	{
		//FULL POWER
	case 1:{
			
			if(millis()>5000+tiempo2){
				tiempo2 = millis();
				if (!WiFi.isConnected()) { Serial.print("Wifi desconectado "); Setup_wifi(); }
				if (WiFi.isConnected()  && !client.connected()) {reconnect();}
			}
			
			client.loop();	
			#if VOLTIMETRO_MODE		
				voltimetro.loop();
			#endif
			#if SENSOR_SUELO_MODE
				if(millis()>(_tiempo_Refresh_ADC+tiempo3)){
					tiempo3 = millis();
					ADCMedia += analogRead(A0);

				}
			#endif
			_valve0.loop();	
			_valve1.loop();	
			_valve2.loop();	
			_valve3.loop();
			//Send data
			if(millis()>30000+tiempo){
				tiempo=millis();
				SendData();
			}
			//Alarmas Riego
			/**/
			yield();
		}
		break;
		//MID_POWER
	case 2:{
		if(millis()>1000+tiempo2){
				tiempo2 = millis();
			if (!WiFi.isConnected()) {  Serial.print("Wifi desconectado ");  Setup_wifi(); }
			if (WiFi.isConnected()  && !client.connected()) {reconnect();}
		}
			//Solo enciende el modo accespoint de ser establecido esto bajara el consumo
			//if(_modeAccesPoint == true){
				//server.handleClient();
			//}

			client.loop();
			voltimetro.loop();
			
			if(millis()>(_tiempo_Send_Data)+tiempo){
				tiempo=millis();
				SendData();
			}
			
			if(millis()>(_tiempo_Activo_MidMode+tiempo3)){
				//Refresh_Time();
				ESP.deepSleep(_tiempo_DeepSleep_MidMode);//= ESP.deepSleep(_tiempo_DeepSleep_MidMode,WAKE_RF_DEFAULT);
				}
			yield();
		}
		break;
		//LOW_POWER
	case 3:{
			Refresh_Time();
			yield();
			}
	break;
	
	default:{
		if(_continue)
			{	
				_continue = false;
				Serial.print("REFRESH modo AccesPoint + STA");
				obtenerWifis();
				accecssPoint();
			}
		server.handleClient();
		break;
		
	}
	
	
	}
	
}