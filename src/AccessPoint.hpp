
#include <ESP8266WiFi.h>
#include <WiFiClient.h> 

#include <ESP8266WebServer.h>
#include <EEPROM.h>


// ESP8266WebServer http_rest_server(8080);
ESP8266WebServer server(80);

struct config_t {
    int timeOn_0,timeOff_0,counterOn_0,counterOff_0,
    timeOn_1,timeOff_1,counterOn_1,counterOff_1,
    timeOn_2,timeOff_2,counterOn_2,counterOff_2,
    timeOn_3,timeOff_3,counterOn_3,counterOff_3;
    char nombre_0[32];
    char nombre_1[32];
    char nombre_2[32];
    char nombre_3[32];
    bool state_riego_0,power_0;
    bool state_riego_1,power_1;
    bool state_riego_2,power_2;
    bool state_riego_3,power_3;
    char wifi[32];
    char pass[64];
    byte mode;
    int decremento_minutos;
} config;

String wifis;
char *nombreRed ;
char *contraWifi ;
char *nombre;

const char *ssidServer = "Configura SmartIrriegation";
const char *passwordServer = "";

#define DEBUG_EEPROM 1

void eepromsave() {
    //digitalWrite(LED_BUILTIN,LOW);
    EEPROM.begin(sizeof(config));
    EEPROM.put(0, config); //Guardo la config
    EEPROM.end();
    //digitalWrite(LED_BUILTIN,HIGH);
}
//Cargo de la eeprom el struct config
void eepromload() {
    //digitalWrite(LED_BUILTIN,0);
    EEPROM.begin(sizeof(config));
    EEPROM.get(0, config); //Cargo config
    EEPROM.end();
    #if DEBUG_EEPROM
      Serial.print("Guardo ssidServer: ");
      Serial.println(config.wifi);  
      Serial.print("Guardo contraseña: ");
      Serial.println(config.pass);
      Serial.print("mode: ");Serial.println(config.mode);
      Serial.println("/////////////////////");
      Serial.println("Riego_0");
      Serial.print("Guardo nombre: ");
      Serial.println(config.nombre_0);
      Serial.print("Guardo timeOn_0: ");
      Serial.println(config.timeOn_0);
      Serial.print("Guardo timeOff_0: ");
      Serial.println(config.timeOff_0);
      Serial.print("Guardo counterOn_0: ");
      Serial.println(config.counterOn_0);
      Serial.print("Guardo counterOff_0: ");
      Serial.println(config.counterOff_0);
      Serial.print("Avalivle_Riego_0: ");
      Serial.println(config.state_riego_0);
      Serial.print("power_0: ");
      Serial.println(config.power_0);
      Serial.println("/////////////////////");
      Serial.println("Riego_1");
      Serial.print("Guardo nombre: ");
      Serial.println(config.nombre_1);
      Serial.print("Guardo timeOn_1: ");
      Serial.println(config.timeOn_1);
      Serial.print("Guardo timeOff_1: ");
      Serial.println(config.timeOff_1);
      Serial.print("STate_Riego_1: ");
      Serial.println(config.state_riego_1);
      Serial.print("power_1: ");
      Serial.println(config.power_1);
       Serial.println("/////////////////////");
      Serial.println("Riego_2");
      Serial.print("Guardo nombre: ");
      Serial.println(config.nombre_2);
      Serial.print("Guardo timeOn_2: ");
      Serial.println(config.timeOn_2);
      Serial.print("Guardo timeOff_2: ");
      Serial.println(config.timeOff_2);
      Serial.print("STate_Riego_2: ");
      Serial.println(config.state_riego_2);
      Serial.print("power_2: ");
      Serial.println(config.power_2);
       Serial.println("/////////////////////");
      Serial.println("Riego_3");
      Serial.print("Guardo nombre: ");
      Serial.println(config.nombre_3);
      Serial.print("Guardo timeOn_3: ");
      Serial.println(config.timeOn_3);
      Serial.print("Guardo timeOff_3: ");
      Serial.println(config.timeOff_3);
      Serial.print("STate_Riego_3: ");
      Serial.println(config.state_riego_3);
      Serial.print("power_3: ");
      Serial.println(config.power_3);
    #endif
    //digitalWrite(LED_BUILTIN,1);
    
}
void eeprominit() {
    Serial.println("Limpiando la EEPROM");
    for (int i = 0 ; i < EEPROM.length() ; i++) {
      //digitalWrite(LED_BUILTIN,0) ;
      if(EEPROM.read(i) != 0){                   
        EEPROM.write(i, 0);  
        // digitalWrite(LED_BUILTIN,1) ;
      }
    }
    
    strncpy(config.wifi, "veredaR", 35);
    strncpy(config.pass, "vereda2019", 64);
  
    config.mode = 1;

    ///////////
    strncpy(config.nombre_0, "Lantana", 32);
    config.timeOff_0 = 71;
    config.timeOn_0 = 1;
    config.state_riego_0= true;
    config.power_0 = true;
    strncpy(config.nombre_2, "Cipres", 32);
    config.timeOff_1 = 12*60;
    config.timeOn_1 = 15;
    config.state_riego_1= false;
    config.power_1 = false;
    strncpy(config.nombre_2, "Huerto", 32);
    config.timeOff_2 = 12*60;
    config.timeOn_2 = 17;
    config.state_riego_2= false;
    config.power_2 = false;
    strncpy(config.nombre_3, "Gallinas", 32);
    config.timeOff_3 = 8*60;
    config.timeOn_3 = 3;
    config.state_riego_3= false;
    config.power_3 = false;

    //Guardo valores por defecto en la eeprom 
    eepromsave(); 
}
int handleLogin(){
  
    String msg;
    //WIFI
    if (server.hasArg("USERNAME") && server.hasArg("PASSWORD")  )
    {
        //digitalWrite(LED_BUILTIN,LOW);
        String message;
        
          //Variables comunes
      
          String wifi2 = server.arg("USERNAME");
          String pass2 = server.arg("PASSWORD");
          
          char buf1[32];
          char buf2[64];
        
          wifi2.toCharArray(buf1, 32);
          pass2.toCharArray(buf2, 64);
          
          strncpy(config.wifi, buf1, 32);
          strncpy(config.pass, buf2, 64);
          
          
          eepromsave(); 
          String mensaje = ("<h1>Gracias por conectarme :)</h1>");
          mensaje += ("<br><p>Reiniciame!</p></br>");
          server.send(301,"text/html", mensaje);
          //digitalWrite(LED_BUILTIN,HIGH);
          Serial.println("Datos enviados y escritos en la eeprom");
      }
      //MODE POWER
     if(server.arg("MODE_POWER") == "LOW_POWER")
     {
       
       Serial.println("Seleccionado mode LOW_POWER");
       config.mode = 3;
        eepromsave(); 
     }
     if(server.arg("MODE_POWER")=="MID_POWER")
     {
       Serial.println("Seleccionado mode MID_POWER");
       config.mode = 2;
        eepromsave(); 
     }
     if(server.arg("MODE_POWER")=="FULL_POWER")
     {
       Serial.println("Seleccionado mode FULL_POWER");
       config.mode = 1;
        eepromsave(); 
     }
    //ACTION DOOR
    if(server.hasArg("DOOR0_ON")){Serial.println("OPEN DOOR 0");OPENDoor(0,delayRele);}
    if(server.hasArg("DOOR0_OFF")){Serial.println("Close DOOR 0");CLOSEDoor(0,delayRele);}
    if(server.hasArg("DOOR1_ON")){Serial.println("OPEN DOOR 1");OPENDoor(1,delayRele);}
    if(server.hasArg("DOOR1_OFF")){Serial.println("Close DOOR 1");CLOSEDoor(1,delayRele);}
    if(server.hasArg("DOOR2_ON")){Serial.println("OPEN DOOR 2");OPENDoor(2,delayRele);}
    if(server.hasArg("DOOR2_OFF")){Serial.println("Close DOOR 2");CLOSEDoor(2,delayRele);}
    if(server.hasArg("DOOR3_ON")){Serial.println("OPEN DOOR 3");OPENDoor(3,delayRele);}
    if(server.hasArg("DOOR3_OFF")){Serial.println("Close DOOR 3");CLOSEDoor(3,delayRele);}


     //
    if(server.hasArg("NOMBRE_0")&& server.hasArg("TIMEON_0")&& server.hasArg("TIMEOFF_0") )
    {
      Serial.print("NOMBRE_0");Serial.println(server.arg("NOMBRE_0"));
       Serial.print("TIMEON_0");Serial.println(server.arg("TIMEON_0"));
        Serial.print("TIMEOFF_0");Serial.println(server.arg("TIMEOFF_0"));
      //Variables Riego 0
        config.timeOn_0 = server.arg("TIMEON_0").toInt();
          config.timeOff_0 = server.arg("TIMEOFF_0").toInt();
          Serial.print("Time on capture: ");
          Serial.println(config.timeOn_0);
          Serial.print("Time off capture: ");
          Serial.println(config.timeOff_0);
          config.state_riego_0 = true;
          String nombre2 = server.arg("NOMBRE_0");
          char buf3[32];
          nombre2.toCharArray(buf3, 32);
          strncpy(config.nombre_0, buf3, 32);
          Serial.print("nombre_0: ");Serial.println(config.nombre_0);
          if(server.arg("DOOR0_AVAILABLE")=="ON")
          {
            config.power_0 = true;
          }else{config.power_0 = false;}
          ///SAVE
          eepromsave(); 
    }
    if(server.hasArg("NOMBRE_1")&& server.hasArg("TIMEON_1")&& server.hasArg("TIMEOFF_1") )
    {
      Serial.print("NOMBRE_1");Serial.println(server.arg("NOMBRE_1"));
       Serial.print("TIMEON_1");Serial.println(server.arg("TIMEON_1"));
        Serial.print("TIMEOFF_1");Serial.println(server.arg("TIMEOFF_1"));
      //Variables Riego 1
        config.timeOn_1 = server.arg("TIMEON_1").toInt();
          config.timeOff_1 = server.arg("TIMEOFF_1").toInt();
          Serial.print("Time on capture");
          Serial.println(config.timeOn_1);
          Serial.print("Time off capture");
          Serial.println(config.timeOff_1);
          config.state_riego_1 = true;
          String nombre2 = server.arg("NOMBRE_1");
          char buf3[32];
          nombre2.toCharArray(buf3, 32);
          strncpy(config.nombre_1, buf3, 32);
          Serial.print("nombre_1: ");Serial.println(config.nombre_1);
          if(server.arg("DOOR1_AVAILABLE")=="ON")
          {
            config.power_1 = true;
          }else{config.power_1 = false;}
          ///SAVE
          eepromsave(); 
    }
    if(server.hasArg("NOMBRE_2")&& server.hasArg("TIMEON_2")&& server.hasArg("TIMEOFF_2") ){
        //Variables Riego 2
         Serial.print("NOMBRE_2");Serial.println(server.arg("NOMBRE_2"));
       Serial.print("TIMEON_2");Serial.println(server.arg("TIMEON_2"));
        Serial.print("TIMEOFF_2");Serial.println(server.arg("TIMEOFF_2"));
        config.timeOn_2 = server.arg("TIMEON_2").toInt();
          config.timeOff_2 = server.arg("TIMEOFF_2").toInt();
          Serial.print("Time on capture");
          Serial.println(config.timeOn_2);
          Serial.print("Time off capture");
          Serial.println(config.timeOff_2);
          config.state_riego_2 = true;
          String nombre2 = server.arg("NOMBRE_2");
          char buf3[32];
          nombre2.toCharArray(buf3, 32);
          strncpy(config.nombre_2, buf3, 32);
          Serial.print("nombre_2: ");Serial.println(config.nombre_2);
          if(server.arg("DOOR2_AVAILABLE")=="ON")
          {
            config.power_2 = true;
          }else{config.power_2 = false;}
          ///SAVE
          eepromsave(); 
      }
    if(server.hasArg("NOMBRE_3")&& server.hasArg("TIMEON_3")&& server.hasArg("TIMEOFF_3") ){
        //Variables Riego 3
         Serial.print("NOMBRE_3");Serial.println(server.arg("NOMBRE_3"));
       Serial.print("TIMEON_3");Serial.println(server.arg("TIMEON_3"));
        Serial.print("TIMEOFF_3");Serial.println(server.arg("TIMEOFF_3"));
        config.timeOn_3 = server.arg("TIMEON_3").toInt();
          config.timeOff_3 = server.arg("TIMEOFF_3").toInt();
          Serial.print("Time on capture");
          Serial.println(config.timeOn_3);
          Serial.print("Time off capture");
          Serial.println(config.timeOff_3);
          config.state_riego_3 = true;
          String nombre2 = server.arg("NOMBRE_3");
          char buf3[32];
          nombre2.toCharArray(buf3, 32);
          strncpy(config.nombre_3, buf3, 32);
          Serial.print("nombre_3: ");Serial.println(config.nombre_3);
          if(server.arg("DOOR3_AVAILABLE")=="ON")
          {
            config.power_3 = true;
          }else{config.power_3 = false;}
          ///SAVE
          eepromsave(); 
      }
    if(server.hasArg("RESTART"))
    {
      delay(2000);
      ESP.restart();
       return 0;
    }
    
    
 
    String content = "<!DOCTYPE html><html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><meta name='viewport' content='width=device-width, initial-scale=1.0'><style>* {  box-sizing:border-box; border-radius: 20px;} .cabeza {font-family: 'calibri';  background-color:#45ADA8;";
    content += "  padding:20px;  float:down;  width:100%;}  .main {    font-family: 'calibri';  background-color:#ffffff;padding-top: 80px; padding-right: 10px;padding-bottom: 140px; padding-left: 10px;  float:down;  width:100%;} .pie {  background-color:#9DE0AD;  text-align: center;  padding:10px; float:down; width:100%; }input,select{  width: 100%;   padding: 12px 20px;    margin: 8px 0;    box-sizing: border-box; border: 3px solid #555;}"; 
    content += ".boton {  background-color: #547980;margin-top: 5%;    border: none; border-radius: 20px; color: black; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px;}  p {  font: 150% sans-serif;} @media screen and (max-width:800px) {  .left, .main, .right {    width:100%;   }}@media only screen and (min-width: 500px) { input,select{  width: 47%;}   }</style></head>";
    content  += "<body> <div class='cabeza'>  <h1>Introduce los datos de la red wifi</h1></div><div class='main'>";
    //WIFI
    content += " <form action='/' method='POST' onsubmit='onEnviar()'> Red Wifi:<br>";
    content += wifis;
    content += " <input type='password' name='PASSWORD' placeholder='Contraseña Wifi' title='Introduce la contraseña de la red wifi'  />";
    content += "  <input type='submit' name='SUBMIT'class='boton' value='OK!'>";
    content += "</form>";
    //MODE POWER
    content += " <form action='/' method='POST' onsubmit='onEnviar()'>";
    content += "<h5>Mode power? </h5> ";
    content += "<label for='ON'>FULL POWER</label>";
    content += " <input type='radio' name='MODE_POWER' value='FULL_POWER' checked><br>";
    content += "<label for='ON'>MID POWER</label>";
    content += " <input type='radio' name='MODE_POWER' value='MID_POWER' ><br>";
    content += "<label for='OFF'>LOW POWER</label>";
    content += " <input type='radio' name='MODE_POWER' value='LOW_POWER' ><br>";
    content += "<br><br><br> FULL POWER: Constantemente conectado sin uso de modos bajo consumo <br> MID POWER:Se conecta a la red, manda los datos y se duerme <br>LOW POWER :Inhabilitara el wifi completamente.<br><br><br>";
   
    
    content += "  <input type='submit' name='SUBMIT'class='boton' value='Save mode'>";
    content += "</form>";

   
    //RIEGO_= 0
    content += "<h2>Riego_0:</h2>";
    content += " <form action='/' method='POST'>";
    content += "<input type='submit' name='DOOR0_ON' value='ON'><br>";
    content += "<input type='submit' name='DOOR0_OFF' value='OFF'><br>";
    content += "</form>";
    content += "<br>";
    content += " <form action='/' method='POST'>";
    content += "<h5>Valvula available? </h5>";
    content += "<label for='ON'>ON</label>";
    content += "<input type='radio' name='DOOR0_AVAILABLE' value='ON'><br>";
    content += "<label for='OFF'>OFF</label>";
    content += "<input type='radio' name='DOOR0_AVAILABLE' value='OFF' checked><br>";
    content += "<br><input type='text' name='NOMBRE_0' placeholder='Nombre Riego_0' required/> ";
    content += "<br><input type='text' name='TIMEON_0' placeholder='Minutos de riego' required/>   ";
    content += "<br><input type='text' name='TIMEOFF_0' placeholder='Minutos de sequia' required/>   ";
    content += "  <input type='submit' name='SUBMIT'class='boton' value='Save riego!'>";
    content += "</form>";
    //RIEGO_1
    content += "<h2>Riego_1:</h2>";
    content += " <form action='/' method='POST'>";
    content += "<input type='submit' name='DOOR1_ON' value='ON'><br>";
    content += "<input type='submit' name='DOOR1_OFF' value='OFF'><br>";
    content += "</form>";
    content += "<br>";
    content += " <form action='/' method='POST' > ";
    content += "<h5>Valvula available? </h5>";
    content += "<label for='ON'>ON</label>";
    content += "<input type='radio' name='DOOR1_AVAILABLE' value='ON'><br>";
    content += "<label for='OFF'>OFF</label>";
    content += "<input type='radio' name='DOOR1_AVAILABLE' value='OFF' checked><br>";
    content += "<br><input type='text' name='NOMBRE_1' placeholder='Nombre Riego_1' required/> ";
    content += "<br><input type='text' name='TIMEON_1' placeholder='Minutos de riego' required/>   ";
    content += "<br><input type='text' name='TIMEOFF_1' placeholder='Minutos de sequia' required/>   ";
    content += "  <input type='submit' name='SUBMIT'class='boton' value='Save riego!'>";
    content += "</form>";
    //RIEGO_2
    content += "<h2>Riego_2:</h2>";
    content += " <form action='/' method='POST'>";
    content += "<input type='submit' name='DOOR2_ON' value='ON'><br>";
    content += "<input type='submit' name='DOOR2_OFF' value='OFF'><br>";
    content += "</form>";
    content += "<br>";
    content += " <form action='/' method='POST' >";
    content += "<h5>Valvula available? </h5>";
    content += "<label for='ON'>ON</label>";
    content += "<input type='radio' name='DOOR2_AVAILABLE' value='ON'><br>";
    content += "<label for='OFF'>OFF</label>";
    content += "<input type='radio' name='DOOR2_AVAILABLE' value='OFF' checked><br>";
    content += "<br><input type='text' name='NOMBRE_2' placeholder='Nombre Riego_2' required/> ";
    content += "<br><input type='text' name='TIMEON_2' placeholder='Minutos de riego' required/>   ";
    content += "<br><input type='text' name='TIMEOFF_2' placeholder='Minutos de sequia' required/>   ";
    content += "  <input type='submit' name='SUBMIT'class='boton' value='Save riego!'>";
    content += "</form>";
   
    //RIEGO_3
    content += "<h2>Riego_3:</h2>";
    content += " <form action='/' method='POST'>";
    content += "<input type='submit' name='DOOR3_ON' value='ON'><br>";
    content += "<input type='submit' name='DOOR3_OFF' value='OFF'><br>";
    content += "</form>";
    content += "<br>";
    content += " <form action='/' method='POST' >";
    content += "<h5>Valvula available? </h5>";
    content += "<label for='ON'>ON</label>";
    content += "<input type='radio' name='DOOR3_AVAILABLE' value='ON'><br>";
    content += "<label for='OFF'>OFF</label>";
    content += "<input type='radio' name='DOOR3_AVAILABLE' value='OFF' checked><br>";
    content += "<br><input type='text' name='NOMBRE_3' placeholder='Nombre Riego_3' required/> ";
    content += "<br><input type='text' name='TIMEON_3' placeholder='Minutos de riego' required/>   ";
    content += "<br><input type='text' name='TIMEOFF_3' placeholder='Minutos de sequia' required/>   ";
    content += "  <input type='submit' name='SUBMIT'class='boton' value='Save riego!'>";
    content += "</form>";

    content += "<input id='date' name='DATE' type='hidden' />";
  
    content += "</div> <div class='pie'>  <p>Powered by SI</p></div></body></html>";
    content += "<script type='text/javascript'>var d = new Date();/*document.write('Fecha: '+d.getDate(),'<br>Dia de la semana: '+d.getDay(),'<br>Mes (1 al 12): '+(d.getMonth()+1),'<br>Año: '+d.getFullYear(),'<br>Hora: '+d.getHours(),'<br>Hora UTC: '+d.getUTCHours(),'<br>Minutos: '+d.getMinutes(),'<br>Segundos: '+d.getSeconds());*/function onEnviar(){       document.getElementById('date').value=d;    }</script>";
   //Serial.println(content); 
    server.send(200, "text/html", content);
  
  
   
 

}
int accecssPoint(){
    //digitalWrite(LED_BUILTIN,HIGH);
    Serial.println();
    Serial.print("Configuring access point...");
    /* You can remove the passwordServer parameter if you want the AP to be open. */
    IPAddress apIP (10, 10, 10, 10); 
    WiFi.softAPConfig( apIP,  apIP, IPAddress(0, 0, 0, 0));
   
    WiFi.softAP(ssidServer, passwordServer);
    IPAddress myIP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(myIP);
    Serial.println("AccessPoint HTTP server started");
    server.begin();
    server.on("/", handleLogin); 
}

void obtenerWifis(){
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  Serial.println("Setup done");
  Serial.println("scan start");
  wifis = "<select type='text' name='USERNAME'>";
  // WiFi.scanNetworks will return the number of networks found
  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  if (n == 0){
    wifis += "<option value='1'>No hay redes disponibles</option></select>";
    Serial.println("no networks found");
  }else  {
    Serial.print(n);
    Serial.println(" networks found");
    for (int i = 0; i < n; ++i)
    {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      Serial.print("Power->");Serial.print(min(max(2 * (WiFi.RSSI(i) + 100), 0), 100));
      Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE)?" ":"*");
      wifis += "<option value='";
      wifis += WiFi.SSID(i);
      wifis += "'>";
      wifis += WiFi.SSID(i);
      wifis += "</option>";
      delay(10);
    }
    wifis += "</select>";
    }
     
}