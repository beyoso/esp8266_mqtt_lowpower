#include <ESP8266WiFi.h>
#define rele_on  1
#define rele_off 0
//////DHT////////
#define DHT_MODE 0
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)
////////////////
#define PLACA01 1
//#define PLACA02 1

#define SENSOR_SUELO_MODE 0

#define VOLTIMETRO_MODE 1
// Update these with values suitable for your network.
uint32_t _tiempo_DeepSleep_Debug = 3e6;
uint32_t _tiempo_DeepSleep_MidMode = 1e6*60*30;//1seg por sesenta segundos de un minuto 60 por 30 minutos =30minutos
unsigned long _tiempo_Activo_MidMode = 12*1000;
unsigned long _tiempo_Send_Data = 3*1000;
uint _tiempo_Refresh_ADC = 100;
const char* ssid = "vereda-R";
const char* password = "vereda2019";
const char* mqtt_server = "192.168.1.100";
const int mqtt_port = 1883;
const char* nameNode = "NodeLowPowerElectroValvula";
const char* controlRiego = "campo/nodeLowPower/control";
const char* actionRiego = "campo/nodeLowPower/action";

int delayRele = 15;

//Puente H
int puente01A1 = D1;
int puente01A2 = D2;
int puente01B1 = D3;
int puente01B2 = D4;
int puente02A1 = D5;
int puente02A2 = D6;
int puente02B1 = D7;
int puente02B2 = D8;

#if PLACA01
//SW7 && SW8
int pinButton = D7;
int pinLed = D8;
int pinDht = D6;
#endif

#if PLACA02
//SW7 && SW8
int pinButton = D1;
int pinLed = D1;
int pinDht = D3;
#endif



