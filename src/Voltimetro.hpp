#include <Arduino.h>
//#include <Ticker.h>  //Ticker Library
//
// Voltimetro_dc.ino
// hispavila.com
// Construcción de un voltímetro DC con Arduino
// 20.09.2015
// Utiliza el monitor Serial para mostrar los valores.
// VCC-----|R1|---(A0)---|R2|------GND
// VCC-----|R1|---(A0)  Menos componenetes mejor calculado, solo valido para wemos D1 and NodeMcu ya que incluyen un puente interno
//


class Voltimetro
  {  private:
         int cont =0 ;
        float V1 = 1.0; // valor real de la alimentacion de Arduino, Vcc
        float R1 = 220000; //220K
        float R2 = 100000; // 100K
        float MEDIA =0;
        unsigned long _dateRefresh = 0;
        unsigned long time =0;
     public:
        ///Sin resistencias añadidas con la malla de casa
         Voltimetro()
        {
            

        }
         Voltimetro( float v1 ,float r1, float r2)
          {
              V1 = v1;
              R1 = r1;
              R2 = r2;
          }
           Voltimetro( float v1 ,float r1, float r2, unsigned long timeRefresh)
          {
              V1 = v1;
              R1 = r1;
              R2 = r2;
              _dateRefresh = timeRefresh;
              time = millis();

          }
        Voltimetro( float r1,  unsigned long timeRefresh)
          {
              V1 = V1;
              R1 = r1+R1;
              R2 = R2;
              _dateRefresh = timeRefresh;
              time = millis();

          }
        void loop()
        {
            if(millis()>_dateRefresh+time){
				time = millis();
                MEDIA += getVoltaje();
                cont++;
            }

        }
        float getMedia()
        {
            float res= (MEDIA/cont);
            MEDIA = 0;
            cont =0;
            return res;
        }  
        void VoltimetroInfo() {
            Serial.println("--------------------");
            Serial.println("DC VOLTMETER");
            Serial.print("Maximum Voltage: ");
            Serial.print((float)(V1 / (R2 / (R1 + R2))));
            Serial.println("V");
            Serial.println("--------------------");
            Serial.println(""); 

            }

            float getVoltaje() {
                float v = (analogRead(A0) * V1) / 1024.0;
                float v2 = v / (R2 / (R1 + R2));

               // Serial.print("V: ");Serial.println(v2);
                //
                return v2;
            }

  } ;




